import { LightningElement } from "lwc";

export default class lightningreset extends LightningElement {
  handleResetAll(){
    this.template.querySelectorAll('lightning-input').forEach(element => {
      if(element.type === 'checkbox' || element.type === 'checkbox-button'){
        element.checked = false;
      }else{
        element.value = null;
      }      
    });
  }

}